FROM openjdk:8-jdk-alpine AS build
WORKDIR /app
COPY . .
RUN chmod +x ./gradlew
RUN ./gradlew clean assemble

FROM openjdk:8-jdk-alpine
COPY --from=build app/build/libs/kafkacomm-1.0.0.jar kafkacomm.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "kafkacomm.jar"]
