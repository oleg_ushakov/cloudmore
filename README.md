### How to start
Just use command `docker-compose up -d` from the project root 

### How to use
After start:
    application available on the port 8080
    database available on the port 3306

We can send POST requests to application for add info to database

`POST http://localhost:8080/api/user`
`Content-Type: application/json`

`{
"name": "Alice",
"surname": "Thompson",
"wage": 5500.75,
"eventTime": "2012-04-23T18:25:43.511Z"
}`

for example for curl we can generate the request as
`curl -X POST -H "Content-Type: application/json" -d '{"name": "Alice","surname": "Thompson","wage": 5500.75,"eventTime": "2012-04-23T18:25:43.511Z"}' http://localhost:8080/api/user`