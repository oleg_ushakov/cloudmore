package oleg.ushakov.kafkacomm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaCommApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaCommApplication.class, args);
    }

}
