package oleg.ushakov.kafkacomm.config;

import oleg.ushakov.kafkacomm.model.dto.kafka.UserDtoMessage;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Oleg Ushakov. 2021
 */
@Configuration
public class KafkaProducerConfig {

    @Value(value = "${kafka.address}")
    private String bootstrapAddress;

    @Bean
    public KafkaTemplate<String, UserDtoMessage> usrMsgKafkaTemplate() {
        return new KafkaTemplate<>(userMsgProducerFactory());
    }

    @Bean
    public ProducerFactory<String, UserDtoMessage> userMsgProducerFactory() {
        Map<String, Object> configProps = new HashMap<>();
        configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        return new DefaultKafkaProducerFactory<>(configProps);
    }
}
