package oleg.ushakov.kafkacomm.controller;

import oleg.ushakov.kafkacomm.model.dto.UserDto;
import oleg.ushakov.kafkacomm.model.dto.Result;
import oleg.ushakov.kafkacomm.service.UserApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Oleg Ushakov. 2021
 */
@RestController
public class UserApiController {

    @Autowired
    private UserApiService userService;

    @PostMapping(value = "/api/user", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Result> statusSync(@RequestBody UserDto userDto) {
        Result result = userService.processMessage(userDto);
        return ResponseEntity.ok(result);
    }

    @GetMapping(value = "/")
    public String index() {
        return "It work's";
    }

}

