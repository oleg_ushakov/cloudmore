package oleg.ushakov.kafkacomm.mapper;

import oleg.ushakov.kafkacomm.model.dto.UserDto;
import oleg.ushakov.kafkacomm.model.dto.kafka.UserDtoMessage;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.time.LocalDateTime;
import java.time.chrono.IsoChronology;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.ResolverStyle;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_TIME;

/**
 * @author Oleg Ushakov. 2021
 */
@Mapper(componentModel = "spring")
public interface UserDtoToUserMessageMapper {

    @Mapping(source = "eventTime", target = "eventTime", qualifiedByName = "convertEventTime()")
    UserDtoMessage convert(UserDto dto);

    @Named("convertEventTime")
    default LocalDateTime convertEventTime(String eventTime) {
        DateTimeFormatter custom = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX");
        return LocalDateTime.parse(eventTime, custom);
    }
}
