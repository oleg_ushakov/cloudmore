package oleg.ushakov.kafkacomm.mapper;

import oleg.ushakov.kafkacomm.model.dto.kafka.UserDtoMessage;
import oleg.ushakov.kafkacomm.model.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * @author Oleg Ushakov. 2021
 */
@Mapper(componentModel = "spring")
public interface UserMessageToEntityMapper {

    @Mapping(target = "id", ignore = true)
    User convert(UserDtoMessage dto);
}
