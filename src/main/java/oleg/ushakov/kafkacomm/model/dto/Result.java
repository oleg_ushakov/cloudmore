package oleg.ushakov.kafkacomm.model.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import oleg.ushakov.kafkacomm.model.ResponceStatus;

/**
 * @author Oleg Ushakov. 2021
 */

@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class Result {

    private ResponceStatus status;
    private String message;

    public static Result ok() {
        return Result.builder()
                .status(ResponceStatus.OK)
                .build();
    }

    public static Result error(String errorMsg) {
        return Result.builder()
                .status(ResponceStatus.ERROR)
                .message(errorMsg)
                .build();
    }
}
