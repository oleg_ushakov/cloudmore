package oleg.ushakov.kafkacomm.model.dto.kafka;

import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;

/**
 * @author Oleg Ushakov. 2021
 */
@Data
public class UserDtoMessage {
    private String name;
    private String surname;
    private BigDecimal wage;
    private LocalDateTime eventTime;
}
