package oleg.ushakov.kafkacomm.model.entity;

import lombok.*;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;

/**
 * @author Oleg Ushakov. 2021
 */
@Entity
@Setter
@Getter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "int(10) unsigned")
    private Long id;
    private String name;
    private String surname;
    private BigDecimal wage;
    private LocalDateTime eventTime;
}
