package oleg.ushakov.kafkacomm.repository;

import oleg.ushakov.kafkacomm.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author Oleg Ushakov. 2021
 */

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findBySurname(String surname);
}
