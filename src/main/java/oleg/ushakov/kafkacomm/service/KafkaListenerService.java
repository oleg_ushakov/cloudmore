package oleg.ushakov.kafkacomm.service;

import oleg.ushakov.kafkacomm.model.dto.kafka.UserDtoMessage;

/**
 * @author Oleg Ushakov. 2021
 */
public interface KafkaListenerService {

    void userListener(UserDtoMessage message);
}
