package oleg.ushakov.kafkacomm.service;

import oleg.ushakov.kafkacomm.model.dto.UserDto;
import oleg.ushakov.kafkacomm.model.dto.Result;

/**
 * @author Oleg Ushakov. 2021
 */

public interface UserApiService {
    Result processMessage(UserDto userDto);
}
