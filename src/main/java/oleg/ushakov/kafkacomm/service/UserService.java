package oleg.ushakov.kafkacomm.service;

import oleg.ushakov.kafkacomm.model.entity.User;
import oleg.ushakov.kafkacomm.model.dto.kafka.UserDtoMessage;

/**
 * @author Oleg Ushakov. 2021
 */
public interface UserService {

    User processor(UserDtoMessage dto);

    User save (UserDtoMessage dto);
}
