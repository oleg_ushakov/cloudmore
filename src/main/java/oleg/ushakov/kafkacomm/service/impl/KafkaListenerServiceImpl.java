package oleg.ushakov.kafkacomm.service.impl;

import oleg.ushakov.kafkacomm.model.dto.kafka.UserDtoMessage;
import oleg.ushakov.kafkacomm.service.KafkaListenerService;
import oleg.ushakov.kafkacomm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaListenerServiceImpl implements KafkaListenerService {

    @Autowired
    UserService userService;

    @Override
    @KafkaListener(
            topics = "${kafka.message.topic.user.name}",
            containerFactory = "userMsgKafkaListenerContainerFactory")
    public void userListener(UserDtoMessage message) {
        System.out.println("Received Message in group foo: " + message);
        userService.processor(message);
    }
}
