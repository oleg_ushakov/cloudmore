package oleg.ushakov.kafkacomm.service.impl;

import oleg.ushakov.kafkacomm.model.dto.kafka.UserDtoMessage;
import oleg.ushakov.kafkacomm.service.KafkaPublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
public class KafkaPublisherServiceImpl implements KafkaPublisherService {

    @Value(value = "${kafka.message.topic.user.name}")
    private String topicName;

    @Autowired
    private KafkaTemplate<String, UserDtoMessage> kafkaTemplate;

    @Override
    public void sendMessage(UserDtoMessage msg) {
        ListenableFuture<SendResult<String, UserDtoMessage>> future = kafkaTemplate.send(topicName, msg);

        future.addCallback(new ListenableFutureCallback<SendResult<String, UserDtoMessage>>() {
            @Override
            public void onSuccess(SendResult<String, UserDtoMessage> result) {
                System.out.printf("Sent message=[%s] with offset=[%s]%n", msg, result.getRecordMetadata().offset());
            }
            @Override
            public void onFailure(Throwable ex) {
                System.out.printf("Unable to send message=[%s] due to : %s %n",msg, ex.getMessage());
            }
        });
    }
}
