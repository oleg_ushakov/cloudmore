package oleg.ushakov.kafkacomm.service.impl;

import oleg.ushakov.kafkacomm.mapper.UserDtoToUserMessageMapper;
import oleg.ushakov.kafkacomm.model.dto.Result;
import oleg.ushakov.kafkacomm.model.dto.UserDto;
import oleg.ushakov.kafkacomm.service.KafkaPublisherService;
import oleg.ushakov.kafkacomm.service.UserApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Oleg Ushakov. 2021
 */
@Service
public class UserApiServiceImpl implements UserApiService {

    @Autowired
    private KafkaPublisherService publisher;

    @Autowired
    UserDtoToUserMessageMapper mapper;

    @Override
    public Result processMessage(UserDto userDto) {
        publisher.sendMessage(mapper.convert(userDto));

        Result ok = Result.ok();
        return ok;
    }
}
