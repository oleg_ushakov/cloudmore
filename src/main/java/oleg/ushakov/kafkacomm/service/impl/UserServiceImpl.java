package oleg.ushakov.kafkacomm.service.impl;

import oleg.ushakov.kafkacomm.mapper.UserMessageToEntityMapper;
import oleg.ushakov.kafkacomm.model.dto.kafka.UserDtoMessage;
import oleg.ushakov.kafkacomm.model.entity.User;
import oleg.ushakov.kafkacomm.repository.UserRepository;
import oleg.ushakov.kafkacomm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author Oleg Ushakov. 2021
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    UserMessageToEntityMapper mapper;

    @Override
    public User processor(UserDtoMessage dto) {
        addWageTax(dto, 0.1);
        return this.save(dto);
    }

    protected UserDtoMessage addWageTax(UserDtoMessage dto, double tax) {
        final BigDecimal wageTaxed = dto.getWage()
                .multiply(BigDecimal.valueOf(1 + tax))
                .setScale(2, RoundingMode.CEILING);
        dto.setWage(wageTaxed);
        return dto;
    }

    @Override
    public User save(UserDtoMessage dto) {
        User convert = mapper.convert(dto);
        return userRepository.save(convert);
    }
}
