package oleg.ushakov.kafkacomm;

import oleg.ushakov.kafkacomm.model.dto.UserDto;
import oleg.ushakov.kafkacomm.model.dto.kafka.UserDtoMessage;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class AbstractTest {
    protected UserDto userDto;
    protected UserDtoMessage userDtoMessage;
    protected LocalDateTime eventTime;
    protected String stringEventTime;
    protected String name;
    protected String surname;
    protected BigDecimal wage;
    protected BigDecimal wageTaxed;

    protected UserDto getUserDto(String name, String surname, BigDecimal wage, String stringEventTime) {
        userDto = new UserDto();
        userDto.setName(name);
        userDto.setSurname(surname);
        userDto.setWage(wage);
        userDto.setEventTime(stringEventTime);
        return userDto;
    }

    protected UserDtoMessage getUserDtoMessage(String name, String surname, BigDecimal wage, LocalDateTime eventTime) {
        userDtoMessage = new UserDtoMessage();
        userDtoMessage.setName(name);
        userDtoMessage.setSurname(surname);
        userDtoMessage.setWage(wage);
        userDtoMessage.setEventTime(eventTime);
        return userDtoMessage;
    }

}
