package oleg.ushakov.kafkacomm.controller;

import oleg.ushakov.kafkacomm.AbstractTest;
import oleg.ushakov.kafkacomm.model.dto.UserDto;
import oleg.ushakov.kafkacomm.service.UserApiService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {UserApiController.class})
@DirtiesContext
class ApiControllerTest extends AbstractTest {

    @Autowired
    MockMvc mvc;

    @MockBean
    UserApiService userApiService;

    @BeforeEach
    void init() {
        wage = BigDecimal.valueOf(100.00);
        name = "Aa";
        surname = "12";
        stringEventTime = "2012-04-23T18:25:43.511Z";
        userDto = getUserDto(name, surname, wage, stringEventTime);
    }

    @Test
    void userRequestTest() throws Exception {
        mvc.perform(post("/api/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(userDto)))
                .andExpect(status().isOk());
    }

    @Test
    void userApiWrongRequestTest() throws Exception {
        mvc.perform(get("/api/user")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(405));
    }

    @Test
    void wrongUrlTest() throws Exception {
        mvc.perform(get("/api/absent")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    private static String asJsonString(final UserDto userDto) {
        try {
            return new ObjectMapper().writeValueAsString(userDto);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}