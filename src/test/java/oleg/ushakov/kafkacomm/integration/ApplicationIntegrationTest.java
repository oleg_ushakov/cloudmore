package oleg.ushakov.kafkacomm.integration;

import oleg.ushakov.kafkacomm.AbstractTest;
import oleg.ushakov.kafkacomm.model.dto.UserDto;
import oleg.ushakov.kafkacomm.model.entity.User;
import oleg.ushakov.kafkacomm.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import org.testcontainers.utility.DockerImageName;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Testcontainers
@SpringBootTest
@AutoConfigureMockMvc
public class ApplicationIntegrationTest extends AbstractTest {

    @Container
    public static MySQLContainer mysql = new MySQLContainer("mysql:5.7")
            .withDatabaseName("apikafka")
            .withUsername("test")
            .withPassword("test");

    @Container
    public static KafkaContainer kafka = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka"))
            .withEmbeddedZookeeper();

    @DynamicPropertySource
    static void properties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", mysql::getJdbcUrl);
        registry.add("spring.datasource.username", mysql::getUsername);
        registry.add("spring.datasource.password", mysql::getPassword);
        registry.add("kafka.address", kafka::getBootstrapServers);
    }

    @Autowired
    UserRepository userRepository;

    @Autowired
    MockMvc mvc;

    @BeforeEach
    void init() {
        wage = BigDecimal.valueOf(5500.75);
        wageTaxed = BigDecimal.valueOf(6050.83);
        name = "Alice";
        surname = "Thompson";
        stringEventTime = "2012-04-23T18:25:43.511Z";
        userDto = getUserDto(name, surname, wage, stringEventTime);
    }

    @Test
    void startToEndTest() throws Exception {
        mvc.perform(post("/api/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(userDto)))
                .andExpect(status().isOk());

        Optional<User> result = userRepository.findBySurname("Thompson");
        assertTrue(result.isPresent());
        result.ifPresent(u -> {
            assertEquals(u.getName(), name);
            assertEquals(u.getSurname(), surname);
            assertEquals(u.getWage(), wageTaxed);
        });
    }

    @Test
    void contextLoads() {
        System.out.println("Context loads!");
    }

    private static String asJsonString(final UserDto userDto) {
        try {
            return new ObjectMapper().writeValueAsString(userDto);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
