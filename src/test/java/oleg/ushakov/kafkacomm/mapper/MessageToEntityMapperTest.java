package oleg.ushakov.kafkacomm.mapper;

import oleg.ushakov.kafkacomm.AbstractTest;
import oleg.ushakov.kafkacomm.model.entity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = {UserMessageToEntityMapperImpl.class})
@DirtiesContext
class MessageToEntityMapperTest extends AbstractTest {

    @Autowired
    UserMessageToEntityMapper underTest;

    @BeforeEach
    void init() {
        wage = BigDecimal.valueOf(100.00);
        name = "Aa";
        surname = "12";
        eventTime = LocalDateTime.of(2021, 3, 20, 14, 30, 50);
        userDtoMessage = getUserDtoMessage(name, surname, wage, eventTime);
    }

    @Test
    void convert() {
        User to = underTest.convert(userDtoMessage);
        assertEquals(wage, to.getWage());
        assertEquals(name, to.getName());
        assertEquals(surname, to.getSurname());
        assertEquals(eventTime, to.getEventTime());
    }
}