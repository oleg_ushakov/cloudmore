package oleg.ushakov.kafkacomm.mapper;

import oleg.ushakov.kafkacomm.AbstractTest;
import oleg.ushakov.kafkacomm.model.dto.kafka.UserDtoMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = {UserDtoToUserMessageMapperImpl.class})
@DirtiesContext
class UserDtoToMessageMapperTest extends AbstractTest {

    @Autowired
    UserDtoToUserMessageMapper underTest;

    @BeforeEach
    void init() {
        wage = BigDecimal.valueOf(100.00);
        name = "Aa";
        surname = "12";
        stringEventTime = "2012-04-23T18:25:43.511Z";
        eventTime = LocalDateTime.of(2012, 4, 23, 18, 25, 43, 511000000);
        userDto = getUserDto(name, surname, wage, stringEventTime);
    }

    @Test
    void convert() {
        UserDtoMessage to = underTest.convert(userDto);
        assertEquals(wage, to.getWage());
        assertEquals(name, to.getName());
        assertEquals(surname, to.getSurname());
        assertEquals(eventTime, to.getEventTime());

    }
}