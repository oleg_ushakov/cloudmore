package oleg.ushakov.kafkacomm.service.impl;

import oleg.ushakov.kafkacomm.model.dto.kafka.UserDtoMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @InjectMocks
    UserServiceImpl underTest;

    UserDtoMessage dto;

    @BeforeEach
    public void init() {
        dto = new UserDtoMessage();
        dto.setWage(BigDecimal.valueOf(100));
    }

    @Test
    void addTax() {
        underTest.addWageTax(dto, 0.15);
        BigDecimal expected = BigDecimal.valueOf(115.00).setScale(2);
        assertEquals(expected, dto.getWage());
    }
}